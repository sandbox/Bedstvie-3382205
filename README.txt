Flag limits

"Flag limiters" module is a supplement to the "Flag" module.

The module provides an event dispatcher to validate the flagging process. In the module implemented only one validation:

maximal count flagging entity per flag type by the specific user.
Also added a notification message about validation errors to the next link types:

Normal link
Ajax link
Confirm form
The validation returns LogicException with code "304 - Not modified".

Similar modules:
Flag limit (Drupal 7)
Flag Limiter (Drupal 7)