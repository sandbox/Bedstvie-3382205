<?php

namespace Drupal\flag_limits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagService;
use Drupal\flag_limits\Event\FlagLimitsEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the FlagLimitsService class that replace FlagService class.
 */
class FlagLimitsService extends FlagService {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Te request stack.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    $request_stack = NULL,
    EventDispatcherInterface $event_dispatcher = NULL
  ) {
    parent::__construct($current_user, $entity_type_manager, $request_stack);

    // Get the event_dispatcher service.
    $this->eventDispatcher = $event_dispatcher ?: \Drupal::service('event_dispatcher');
  }

  /**
   * {@inheritdoc}
   */
  public function flag(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {
    $this->ensureSession();
    $this->populateFlaggerDefaults($account, $session_id);

    $event = new FlagLimitsEvent($flag, $entity, $account, $session_id);
    $this->eventDispatcher->dispatch($event, FlagLimitsEvent::FLAG_LIMITS_FLAG_VALIDATE);

    if ($event->isNotValid()) {
      $message = $event->getUserErrorMessages();
      if ($message) {
        throw new \LogicException($message, $event::ERROR_CODE_SHOW_MESSAGE);
      }
      else {
        throw new \LogicException($event->getErrorMessages());
      }
    }

    return parent::flag($flag, $entity, $account, $session_id);
  }

}
