<?php

namespace Drupal\flag_limits;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the flag service.
 */
class FlagLimitsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('flag')) {
      $definition = $container->getDefinition('flag');
      $definition->setClass('Drupal\flag_limits\FlagLimitsService')
        ->addArgument(new Reference('event_dispatcher'));
    }
  }

}
