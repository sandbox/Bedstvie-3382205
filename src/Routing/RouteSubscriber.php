<?php

namespace Drupal\flag_limits\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides a flag limits implementation for RouteSubscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $controlers = [
      'flag.action_link_flag' => 'FlagLimitsActionLinkController::flag',
      'flag.action_link_flag_nojs' => 'FlagLimitsActionLinkNoJsController::flag',
    ];
    foreach ($controlers as $route => $controler) {
      if ($route = $collection->get($route)) {
        $route->setDefault('_controller', '\\Drupal\\flag_limits\\Controller\\' . $controler);
      }
    }

    if ($route = $collection->get('flag.confirm_flag')) {
      $route->setDefault('_form', '\Drupal\flag_limits\Form\FlagLimitsConfirmForm');
    }
  }

}
