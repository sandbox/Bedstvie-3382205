<?php

namespace Drupal\flag_limits\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\FlagInterface;

/**
 * Event that validate limits for flag.
 */
class FlagLimitsEvent extends Event {

  const FLAG_LIMITS_FLAG_VALIDATE = 'flag_limits_flag_validate';

  /**
   * Code: 304 - Not Modified.
   *
   * Use this code when need to display error message for user.
   */
  const ERROR_CODE_SHOW_MESSAGE = 304;

  /**
   * Use this code when do not need to display error message for user.
   */
  const ERROR_CODE_SKIP_MESSAGE = 0;

  /**
   * The flag.
   *
   * @var \Drupal\flag\FlagInterface
   */
  protected $flag;

  /**
   * The flag entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The anonymous session ID.
   *
   * @var string|null
   */
  protected $anonymousSessionId;

  /**
   * Stores flag validation errors.
   *
   * @var array
   */
  protected $validationErrors = [];

  /**
   * Constructs the object.
   *
   * @param \Drupal\flag\FlagInterface $flag
   *   The flag entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The flaggable entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account of flagging.
   * @param string|null $session_id
   *   The session ID.
   */
  public function __construct(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {
    $this->entity = $entity;
    $this->flag = $flag;
    $this->account = $account;
    $this->anonymousSessionId = $session_id;
  }

  /**
   * Get flag.
   *
   * @return \Drupal\flag\FlagInterface
   *   Flag.
   */
  public function getFlag(): FlagInterface {
    return $this->flag;
  }

  /**
   * Set flag.
   *
   * @param \Drupal\flag\FlagInterface $flag
   *   Flag.
   */
  public function setFlag(FlagInterface $flag): void {
    $this->flag = $flag;
  }

  /**
   * Get flagging entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Flagging entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Set flagging entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Flagging entity.
   */
  public function setEntity(EntityInterface $entity): void {
    $this->entity = $entity;
  }

  /**
   * Get flagging User.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   User that flagging entity.
   */
  public function getAccount(): ?AccountInterface {
    return $this->account;
  }

  /**
   * Set flagging User.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   User that flagging entity.
   */
  public function setAccount(?AccountInterface $account): void {
    $this->account = $account;
  }

  /**
   * Get anonymous Session ID.
   *
   * @return string|null
   *   Anonymous Session ID.
   */
  public function getAnonymousSessionId(): ?string {
    return $this->anonymousSessionId;
  }

  /**
   * Set anonymous Session ID.
   *
   * @param string|null $anonymousSessionId
   *   Anonymous Session ID.
   */
  public function setAnonymousSessionId(?string $anonymousSessionId): void {
    $this->anonymousSessionId = $anonymousSessionId;
  }

  /**
   * Return validation errors array.
   *
   * @return array
   *   Validation errors.
   */
  public function getValidationErrors() {
    return $this->validationErrors;
  }

  /**
   * Return validation status.
   *
   * @return bool
   *   Status.
   */
  public function isNotValid() {
    return count($this->getValidationErrors());
  }

  /**
   * Get error messages.
   *
   * @return string
   *   Compared error messages.
   */
  public function getErrorMessages() {
    $error_messages = [];
    foreach ($this->getValidationErrors() as $error) {
      if (is_array($error) && !empty($error['message'])) {
        $error_messages[] = $error['message'];
      }
      else {
        $error_messages[] = $error;
      }
    }

    return implode(' | ', $error_messages);
  }

  /**
   * Get error messages that displayed for user.
   *
   * @return string
   *   Compared error messages with code ERROR_CODE_SHOW_MESSAGE.
   */
  public function getUserErrorMessages() {
    $error_messages = [];
    foreach ($this->getValidationErrors() as $error) {
      if (is_array($error) && !empty($error['code']) && !empty($error['message']) && $error['code'] == $this::ERROR_CODE_SHOW_MESSAGE) {
        $error_messages[] = $error['message'];
      }
    }

    return implode(' | ', $error_messages);
  }

  /**
   * Set validation error.
   *
   * @param string $error
   *   Error message.
   * @param int $code
   *   Http error code.
   *   ERROR_CODE_SHOW_MESSAGE - 304
   *   ERROR_CODE_SKIP_MESSAGE - 0.
   */
  public function setError(string $error, int $code = self::ERROR_CODE_SKIP_MESSAGE) {
    $this->validationErrors[] = [
      'message' => $error,
      'code' => $code,
    ];
  }

}
